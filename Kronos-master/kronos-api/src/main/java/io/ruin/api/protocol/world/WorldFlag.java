package io.ruin.api.protocol.world;

public enum WorldFlag {
    USA,
    UK,
    CANADA,
    AUSTRALIA,
    NETHERLANDS,
    SWEDEN,
    TURKEY, // was FINLAND, switched for TURKEY TODO: Edit flag to Turkey, also on SpriteID.java
    GERMANY
}