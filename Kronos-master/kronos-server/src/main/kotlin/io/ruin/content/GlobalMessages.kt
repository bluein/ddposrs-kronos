package io.ruin.content

import io.ruin.api.globalEvent
import io.ruin.cache.Icon
import io.ruin.utility.Broadcast

/**
 * @author Leviticus
 */
object GlobalMessages {

    init {
        globalEvent {
            while (true) {
                pause(1400) // Every 15 minutes.
                announce()
            }
        }
    }

    private fun announce() {
        Broadcast.WORLD.sendNews(Icon.ANNOUNCEMENT, "You are playing DDP OSRS! This message is a placeholder until Dev Bluein replaces it.")
    }
}