# DDP OSRS (Kronos Fork)
![Kronos Logo](https://imgur.com/vlOi8YW.png)

This is a fork of Kronos edited for the DDP OSRS project. As time goes on, it will be edited heavily for DDP.

## Known Issues 
* Server.java currently compiles, but is not tested fully.
* While creating a new server.properties, make sure that the capitalization of ALL file paths are accurate. This will cause the server to fail compiling or running in most environments.
* Central Server compiles and runs, but currently accesses a defunct external server for the world list.
    * TODO: Analyse code for how it grabs server data off the .php, and recreate feature.
* A lot of the code is very badly written. Lots of cleanup is required, but not the current goal.
* The client has not been changed yet. Nothing is confirmed to be working or broken.
---

**This was originally released on [Rune-Server](https://www.rune-server.ee/runescape-development/rs2-server/downloads/696766-kronos-osrs-semi-custom-server-deob-client-multi-world-support-184-a.html).**

---

> Kronos was a semi-custom server that was hosted from May to September of 2020. It launched with 150 players online and maintained stability well.
> The goal was to have the features of a modern deob client such as gpu rendering while also having high quality custom content.
> Most notably is the way item attributes and upgrades work. On each item object there is also an optional list of attributes that can be added fairly easily.
> This server contains most content up to TOB and there is a good bit of work put on it before closing.
